# node_search

#### 介绍
nodejs实现的文件搜索api


#### 安装教程

1.  nodejs环境
2.  npm install -g nodemon  （nodemon方便调试）
3.  nodemon server.js ./   （启动服务 后面配置搜索目录 ”/“结尾）
4.  VScode 接口调试 安装 `REST Client`  / 也可以用别的方式调试
5.  启用服务 node server.js ./  (./为文件根目录)
5.  POST请求格式  http://127.0.0.1:8088/search 

```
//http://127.0.0.1:8088/search 

{
    "root":"test/",
    "name":"文件"
}
```