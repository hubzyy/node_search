const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();
const search = require('./search')

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cors());
app.use(search())



app.listen(8088, () => {
	console.log('http://127.0.0.1:8088');
})